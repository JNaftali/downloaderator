import csv
import re
from pathlib import Path
import shutil
import requests
import argparse
import sys

parser = argparse.ArgumentParser(
    description="Parse a csv and download files listed in a properly formatted column"
)
parser.add_argument("file", nargs="?", type=argparse.FileType("r"), default=sys.stdin)
parser.add_argument("-c", "--datacolumn", type=str, default="Source File")
parser.add_argument("-f", "--foldercolumn", type=str, default="Product Code")
args = parser.parse_args()


f = args.file
reader = csv.DictReader(f)

for row in reader:
    sources = row.get(args.datacolumn).split(",")
    product_code = row.get(args.foldercolumn)
    for rawsource in sources:
        if not rawsource:
            continue
        m = re.match(r"(?P<name>.+?) \((?P<url>.*?)\)", rawsource)
        if not m:
            print(f"wtf is {rawsource}")
            continue
        # print(f"Name: {m.group('name')}, Url: {m.group('url')}")
        response = requests.get(m.group("url"), stream=True)
        folder = Path("results", product_code)
        folder.mkdir(parents=True, exist_ok=True)
        dest = folder / Path(m.group("name"))
        dest.touch()

        with open(dest, "wb") as out_file:
            shutil.copyfileobj(response.raw, out_file)
